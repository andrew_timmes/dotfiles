#!/bin/bash

# Warn user this script will overwrite current dotfiles
while true; do
  read -p "Warning: this will overwrite your current dotfiles. Continue? [y/n] " yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

# Get the dotfiles directory's absolute path
SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
#DOTFILES_DIR="$(dirname "$SCRIPT_DIR")"
DOTFILES_DIR=$SCRIPT_DIR

git submodule init
git submodule update --init --recursive

ln -nfs $DOTFILES_DIR/.colors ~/.colors
ln -nfs $DOTFILES_DIR/.colors/dircolors-solarized/dircolors.256dark ~/.dir_colors
ln -nfs $DOTFILES_DIR/.tmux ~/.tmux
ln -nfs $DOTFILES_DIR/.tmux/.tmux.conf ~/.tmux.conf
ln -nfs $DOTFILES_DIR/.tmux.conf.local ~/.tmux.conf.local
ln -nfs $DOTFILES_DIR/.zprezto ~/.zprezto
ln -nfs $DOTFILES_DIR/.zprezto/runcoms/zlogin ~/.zlogin
ln -nfs $DOTFILES_DIR/.zprezto/runcoms/zlogout ~/.zlogout
ln -nfs $DOTFILES_DIR/.zprezto/runcoms/zpreztorc ~/.zpreztorc
ln -nfs $DOTFILES_DIR/.zprezto/runcoms/zprofile ~/.zprofile
ln -nfs $DOTFILES_DIR/.zprezto/runcoms/zshenv ~/.zshenv
ln -nfs $DOTFILES_DIR/.zprezto/runcoms/zshrc ~/.zshrc
ln -nfs $DOTFILES_DIR/.vim ~/.vim
ln -nfs $DOTFILES_DIR/.vimrc ~/.vimrc
