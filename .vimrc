set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'SirVer/ultisnips'
Bundle 'rodjek/vim-puppet'
Bundle 'godlygeek/tabular'
Bundle 'scrooloose/syntastic'
filetype plugin indent on

map ,, :Ta /=><CR>/;<CR>/;<CR>


set smartindent
set softtabstop=2
set shiftwidth=2
set expandtab
set sw=2
set sts=2
set backspace=indent,eol,start
set number

cmap w!! w !sudo tee > /dev/null %
syntax enable
set background=dark
let g:solarized_termtrans = 1
let g:syntastic_python_checkers = ['flake8', 'pep8', 'mypy', 'pylint', 'pyright']
colorscheme solarized
